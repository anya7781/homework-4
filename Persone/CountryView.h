//
//  CountryView.h
//  Lection6
//
//  Created by Admin on 14.11.16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface CountryView : UIView

@property (nonatomic, strong) UITableView *table;

@property (nonatomic, strong) UITextField *searchField;
@property (nonatomic, strong) UIButton *searchButton;

@end
