//
//  CustomView.h
//  Lection6
//
//  Created by Vladislav Grigoriev on 03/11/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface CustomView : UIView

@property (strong, nonatomic) UILabel *Name;
@property (strong, nonatomic) UILabel *SurName;
@property (strong, nonatomic) UILabel *FatherName;
@property (strong, nonatomic) UILabel *Email;
@property (strong, nonatomic) UILabel *Phone;
@property (strong, nonatomic) UILabel *Country;
@property (strong, nonatomic) UIButton *Image;

@property (strong, nonatomic) UIButton *Edit;

@property (nonatomic, assign) CGFloat topOffset;

@property (strong, nonatomic) UILabel *StringCountry;
@property (strong, nonatomic) UILabel *StringEmail;
@property (strong, nonatomic) UILabel *StringPhone;



@end
