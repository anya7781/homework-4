//
//  EditViewController.h
//  Lection6
//
//  Created by Vladislav Grigoriev on 07/11/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Model.h"


@protocol  EditViewControllerDelegate;

@interface EditViewController : UIViewController

@property (nonatomic, copy) Model *item;
@property (nonatomic, copy) NSString *ChangedCountry;
@property BOOL b;


@end
