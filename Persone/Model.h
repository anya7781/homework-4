//
//  Item.h
//  Lection6
//
//  Created by Admin on 12.11.16.
//  Copyright © 2016 Inostudio. All rights reserved.
//


#import <Foundation/Foundation.h>

@interface Model : NSObject  <NSCopying, NSCoding>

@property (strong, nonatomic) Model *item;

@property (strong, nonatomic) NSString *Name;
@property (strong, nonatomic) NSString *SurName;
@property (strong, nonatomic) NSString *FatherName;
@property (strong, nonatomic) NSString *Email;
@property (strong, nonatomic) NSString *Phone;
@property (strong, nonatomic) NSString *Country;
@property (strong, nonatomic) NSString *NameImage;

- (void)save;
- (void)load;


@end


