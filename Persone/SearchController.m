//
//  SearchController.m
//  Lection6
//
//  Created by Admin on 15.11.16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "SearchController.h"
#import "CountryView.h"
#import "CountryController.h"
#import "EditViewController.h"

@interface SearchController () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) CountryView *countryView;
@property (nonatomic, strong) NSMutableArray *countries;



@end

@implementation SearchController

- (void)loadView {
    self.countryView = [[CountryView alloc] init];
    self.view = self.countryView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.countryView.table.dataSource = self;
    self.countryView.table.delegate = self;
    
    _countries = [[NSMutableArray alloc] init];
    NSLocale *currentLocale = [NSLocale currentLocale];
    for (NSString *code in [NSLocale ISOCountryCodes]){
        NSString *localizedCountry = [currentLocale localizedStringForCountryCode: code];
        if (localizedCountry){
            [_countries addObject: localizedCountry];
        }
    };
    
    _searchCountries = [[NSMutableArray alloc] init];
    NSInteger count = self.countries.count;
    
    for (NSInteger i = 0; i < count; i++) {
        NSArray *phone = [[self.countries objectAtIndex:i] componentsSeparatedByString:self.searchField.text];
        NSInteger integ = phone.count;
        if (integ > 1)
            [_searchCountries addObject:[self.countries objectAtIndex:i]];
        
    }
    
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (_searchCountries.count == 0) return 1;
    return _searchCountries.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MYCell"];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MYCell"];
    }
    
    if (_searchCountries.count > 0)
        cell.textLabel.text = [_searchCountries objectAtIndex:indexPath.row];
    else
        cell.textLabel.text = @"Not Found";
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    EditViewController *editController = [[EditViewController alloc] init];
    
    editController.ChangedCountry = [_searchCountries objectAtIndex:indexPath.row];
    editController.b = NO;
    editController.item = _item;
    
    [self.navigationController pushViewController:editController animated:YES];
}




@end
