//
//  SearchController.h
//  Lection6
//
//  Created by Admin on 15.11.16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Model.h"

@interface SearchController : UIViewController

@property (nonatomic, strong) NSMutableArray *searchCountries;

@property (nonatomic, strong) UITextField *searchField;
@property (nonatomic, copy) Model *item;

@end
