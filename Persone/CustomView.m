//
//  CustomView.m
//  Lection6
//
//  Created by Vladislav Grigoriev on 03/11/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "CustomView.h"

@implementation CustomView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        
        _Image = [[UIButton alloc] init];
        [self addSubview:_Image];
        
        _Name = [[UILabel alloc] init];
        [self addSubview:_Name];
        
        _SurName = [[UILabel alloc] init];
        [self addSubview:_SurName];
        
        _Edit = [[UIButton alloc] init];
        [_Edit setTitle:@"Edit" forState:UIControlStateNormal];
        _Edit.backgroundColor = [UIColor redColor];
        [self addSubview:_Edit];
        
        _FatherName = [[UILabel alloc] init];
        [self addSubview:_FatherName];
        
        _Email = [[UILabel alloc] init];
        [self addSubview:_Email];
        
        _Phone = [[UILabel alloc] init];
        [self addSubview:_Phone];
        
        _Country = [[UILabel alloc] init];
        [self addSubview:_Country];
        
        
        _StringEmail = [[UILabel alloc] init];
        _StringEmail.text = @"E-mail:";
        [self addSubview:_StringEmail];
        
        _StringPhone = [[UILabel alloc] init];
        _StringPhone.text = @"Phone:";
        [self addSubview:_StringPhone];
        
        _StringCountry = [[UILabel alloc] init];
        _StringCountry.text = @"Country:";
        [self addSubview:_StringCountry];
        
        
    }
    return self;
}


- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat offset = 40.0f;
    
    self.Image.frame = CGRectMake(offset, offset + 30, 160.0f, 200.0f);
    
    self.Edit.frame = CGRectMake(self.bounds.size.width/2 - 30, 3*self.bounds.size.height/4, 60, 40);
    
    self.Name.frame = CGRectMake(self.Image.frame.origin.x + self.Image.bounds.size.width + offset, self.topOffset +  offset + (self.Image.bounds.size.height - 50.0f) / 2.0f, self.bounds.size.width - self.Image.frame.origin.x - self.Image.bounds.size.width - offset, 50.0f);
    
    self.SurName.frame = CGRectMake(self.Image.frame.origin.x + self.Image.bounds.size.width + offset, self.topOffset +  offset + (self.Image.bounds.size.height - 50.0f) / 2.0f, self.bounds.size.width - self.Image.frame.origin.x - self.Image.bounds.size.width - offset, 120.0f);
    
    self.FatherName.frame = CGRectMake(self.Image.frame.origin.x + self.Image.bounds.size.width + offset, self.topOffset +  offset + (self.Image.bounds.size.height - 50.0f) / 2.0f, self.bounds.size.width - self.Image.frame.origin.x - self.Image.bounds.size.width - offset, 190.0f);
    
    self.Email.frame = CGRectMake(offset + 80,CGRectGetMaxY(self.Image.frame), self.bounds.size.width - offset * 2, 50);
    self.StringEmail.frame = CGRectMake(offset,CGRectGetMaxY(self.Image.frame), 80, 50);
    
    self.Phone.frame = CGRectMake(offset + 80,CGRectGetMaxY(self.Email.frame), self.bounds.size.width - offset * 2, 50);
    self.StringPhone.frame = CGRectMake(offset,CGRectGetMaxY(self.Email.frame), 80, 50);
    
    self.Country.frame = CGRectMake(offset + 80,CGRectGetMaxY(self.Phone.frame), self.bounds.size.width - offset * 2, 50);
    self.StringCountry.frame = CGRectMake(offset,CGRectGetMaxY(self.Phone.frame), 80, 50);
    
    
    
}




@end
