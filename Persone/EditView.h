//
//  EditView.h
//  Lection6
//
//  Created by Vladislav Grigoriev on 07/11/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EditView : UIView

@property (nonatomic, strong) UIButton *iconButton;

@property (nonatomic, strong) UITextField *editName;
@property (nonatomic, strong) UITextField *editSurName;
@property (nonatomic, strong) UITextField *editFatherName;
@property (nonatomic, strong) UITextField *editEmail;
@property (nonatomic, strong) UITextField *editPhone;
@property (nonatomic, strong) UILabel *editCountry;

@property (nonatomic, strong) UIButton *Change;

@property (nonatomic, strong) UIButton *editImage;

@property (nonatomic, strong) UILabel *stringName;
@property (nonatomic, strong) UILabel *stringSurName;
@property (nonatomic, strong) UILabel *stringFatherName;
@property (nonatomic, strong) UILabel *stringEmail;
@property (nonatomic, strong) UILabel *stringPhone;
@property (nonatomic, strong) UILabel *stringCountry;

@property (nonatomic, strong) UILabel *error;

@property (nonatomic, assign) CGFloat topOffset;

@end
