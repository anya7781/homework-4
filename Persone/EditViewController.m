//
//  EditViewController.m
//  Lection6
//
//  Created by Vladislav Grigoriev on 07/11/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "EditViewController.h"
#import "EditView.h"
#import "ViewController.h"
#import "CountryController.h"
#import "SearchController.h"
#import "ImageViewController.h"



@interface EditViewController () <UITextFieldDelegate, UITextViewDelegate>

@property (nonatomic, strong) EditView *editView;


@end

@implementation EditViewController

- (void)loadView {
    self.editView = [[EditView alloc] init];
    self.view = self.editView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
       if (!self.item) {
        _item = [[Model alloc] init];
    }
    
    
     
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStyleDone target:self action:@selector(saveItem:)];
    
    
    self.editView.editName.text = _item.Name;
    self.editView.editSurName.text = _item.SurName;
    self.editView.editFatherName.text = _item.FatherName;
    self.editView.editEmail.text = _item.Email;
    self.editView.editPhone.text = _item.Phone;
    
    if (_b == NO)
        self.editView.editCountry.text = _ChangedCountry;
    else
        self.editView.editCountry.text = _item.Country;
    
    [self.editView.Change addTarget:self action:@selector(changeCountry:) forControlEvents:UIControlEventTouchUpInside];
    
    if (_item != nil){
        [_editView.editImage setImage:[UIImage imageNamed:_item.NameImage] forState:UIControlStateNormal];
    }
    
    [self.editView.editImage addTarget:self action:@selector(ImageEdit:) forControlEvents:UIControlEventTouchUpInside];
    
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    self.editView.topOffset = self.topLayoutGuide.length;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *text = [textField.text stringByReplacingCharactersInRange:range withString:string];
    return text.length <= 10;
}

- (void)saveItem:(id)sender {
    
    NSArray *phone = [self.editView.editEmail.text componentsSeparatedByString:@"@"];
    NSInteger i = phone.count;
    
    if (self.editView.editName.text.length == 0 && self.editView.editSurName.text.length == 0 && self.editView.editFatherName.text.length == 0 && self.editView.editEmail.text.length == 0 && self.editView.editPhone.text.length == 0){
        
        self.editView.error.text = @"Some fields are empty";
        
    }
    
    else if (![self.editView.editPhone.text hasPrefix:@"+"] || self.editView.editPhone.text.length < 12)
        self.editView.error.text = @"Incorrect format of the phone number";
    
    else if (i != 2){
        
        self.editView.error.text = @"Incorrect format of the email";
        
    }
    
    else {
        
        self.item.Name = self.editView.editName.text;
        self.item.SurName = self.editView.editSurName.text;
        self.item.FatherName = self.editView.editFatherName.text;
        self.item.Email = self.editView.editEmail.text;
        self.item.Phone = self.editView.editPhone.text;
        self.item.Country = self.editView.editCountry.text;
        //self.item.NameImage = self.editView.editImage
        
        
        ViewController *viewController = [[ViewController alloc] init];
        viewController.item = _item;
        
        [self.item save];
        
        
        [self.navigationController pushViewController:viewController animated:YES];
    }
    
}

-(void) changeCountry:(id) sender {
    
    self.item.Name = self.editView.editName.text;
    self.item.SurName = self.editView.editSurName.text;
    self.item.FatherName = self.editView.editFatherName.text;
    self.item.Email = self.editView.editEmail.text;
    self.item.Phone = self.editView.editPhone.text;
    
    
    CountryController *countryController = [[CountryController alloc] init];
    countryController.item = _item;
    
    [self.navigationController pushViewController:countryController animated:YES];
    
}

-(void) ImageEdit: (id) sender {
    
    ImageViewController *controller = [[ImageViewController alloc] init];
    [self.navigationController pushViewController:controller animated:YES];
    
    
}




@end
