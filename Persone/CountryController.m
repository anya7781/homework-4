//
//  CountryController.m
//  Lection6
//
//  Created by Admin on 14.11.16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "CountryController.h"
#import "CountryView.h"
#import "EditViewController.h"
#import "ViewController.h"
#import "SearchController.h"

@interface CountryController () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) CountryView *countryView;

@end



@implementation CountryController

- (void)loadView {
    self.countryView = [[CountryView alloc] init];
    self.view = self.countryView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.countryView.table.dataSource = self;
    self.countryView.table.delegate = self;
    
    [self.countryView.searchButton addTarget:self action:@selector(search:) forControlEvents:UIControlEventTouchUpInside];
    
    
    _countries = [[NSMutableArray alloc] init];
    NSLocale *currentLocale = [NSLocale currentLocale];
    for (NSString *code in [NSLocale ISOCountryCodes]){
        NSString *localizedCountry = [currentLocale localizedStringForCountryCode: code];
        if (localizedCountry){
            [_countries addObject: localizedCountry];
        }
    };
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.countries.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MYCell"];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MYCell"];
    }
    
    cell.textLabel.text = [_countries objectAtIndex:indexPath.row];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    EditViewController *editController = [[EditViewController alloc] init];
    
    editController.ChangedCountry = [_countries objectAtIndex:indexPath.row];
    editController.b = NO;
    editController.item = _item;
    
    [self.navigationController pushViewController:editController animated:YES];
}

-(void) search: (id) sender {
    
    if (![_countryView.searchField.text isEqualToString:@""]) {
        SearchController *searchController = [[SearchController alloc] init];
        
        searchController.searchField = _countryView.searchField;
        searchController.searchField.text = _countryView.searchField.text;
        searchController.item = _item;
        
        [self.navigationController pushViewController:searchController animated:YES];
    }
    
}


@end
