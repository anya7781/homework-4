
//
//  EditView.m
//  Lection6
//
//  Created by Vladislav Grigoriev on 07/11/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "EditView.h"

@implementation EditView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        
        
        
        
        _editName = [[UITextField alloc] init];
        [self addSubview:_editName];
        
        _editSurName = [[UITextField alloc] init];
        [self addSubview:_editSurName];
        
        _editFatherName = [[UITextField alloc] init];
        [self addSubview:_editFatherName];
        
        _editEmail = [[UITextField alloc] init];
        [self addSubview:_editEmail];
        
        _editPhone = [[UITextField alloc] init];
        [self addSubview:_editPhone];
        
        _editCountry = [[UILabel alloc] init];
        // _editCountry.text = @"Russia";
        [self addSubview:_editCountry];
        
        _editImage = [[UIButton alloc] init];
        [self addSubview:_editImage];
        
        _Change = [[UIButton alloc] init];
        [_Change setTitle:@"Change" forState:UIControlStateNormal];
        _Change.backgroundColor = [UIColor redColor];
        [self addSubview:_Change];
        
        _error = [[UILabel alloc] init];
        [self addSubview:_error];
        
        
        
        _stringName = [[UILabel alloc] init];
        _stringName.text = @"First Name:";
        [self addSubview:_stringName];
        
        _stringSurName = [[UILabel alloc] init];
        _stringSurName.text = @"Last Name:";
        [self addSubview:_stringSurName];
        
        _stringFatherName = [[UILabel alloc] init];
        _stringFatherName.text = @"Middle Name:";
        [self addSubview:_stringFatherName];
        
        _stringEmail = [[UILabel alloc] init];
        _stringEmail.text = @"E-mail:";
        [self addSubview:_stringEmail];
        
        _stringPhone = [[UILabel alloc] init];
        _stringPhone.text = @"Phone:";
        [self addSubview:_stringPhone];
        
        _stringCountry = [[UILabel alloc] init];
        _stringCountry.text = @"Country:";
        [self addSubview:_stringCountry];
        
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat offset = 20.0f;
    
    self.editImage.frame = CGRectMake(self.bounds.size.width/2 - 80.0f, offset + 50, 160.0f, 200.0f);
    
    self.editName.frame = CGRectMake(offset + 110,CGRectGetMaxY(self.editImage.frame) + offset, self.bounds.size.width - offset * 2 - 100, 20);
    self.editName.borderStyle = UITextBorderStyleRoundedRect;
    
    self.stringName.frame = CGRectMake(offset,CGRectGetMaxY(self.editImage.frame) + offset, 90, 20);
    self.editName.borderStyle = UITextBorderStyleRoundedRect;
    
    self.editSurName.frame = CGRectMake(offset + 110,CGRectGetMaxY(self.editName.frame) + offset, self.bounds.size.width - offset * 2 - 100, 20);
    self.editSurName.borderStyle = UITextBorderStyleRoundedRect;
    
    self.stringSurName.frame = CGRectMake(offset,CGRectGetMaxY(self.editName.frame) + offset, 90, 20);
    self.editSurName.borderStyle = UITextBorderStyleRoundedRect;
    
    self.editFatherName.frame = CGRectMake(offset + 110,CGRectGetMaxY(self.editSurName.frame) + offset, self.bounds.size.width - offset * 2 - 100, 20);
    self.editFatherName.borderStyle = UITextBorderStyleRoundedRect;
    self.stringFatherName.frame = CGRectMake(offset,CGRectGetMaxY(self.editSurName.frame) + offset, 120, 20);
    
    self.editEmail.frame = CGRectMake(offset + 110,CGRectGetMaxY(self.editFatherName.frame) + offset, self.bounds.size.width - offset * 2 - 100, 20);
    self.editEmail.borderStyle = UITextBorderStyleRoundedRect;
    self.stringEmail.frame = CGRectMake(offset,CGRectGetMaxY(self.editFatherName.frame) + offset, 90, 20);
    
    self.editPhone.frame = CGRectMake(offset + 110,CGRectGetMaxY(self.editEmail.frame) + offset, self.bounds.size.width - offset * 2 - 100, 20);
    self.editPhone.borderStyle = UITextBorderStyleRoundedRect;
    self.stringPhone.frame = CGRectMake(offset,CGRectGetMaxY(self.editEmail.frame) + offset, 90, 20);
    
    self.editCountry.frame = CGRectMake(offset + 110,CGRectGetMaxY(self.editPhone.frame) + offset, self.bounds.size.width - offset * 2 - 100, 20);
    self.stringCountry.frame = CGRectMake(offset,CGRectGetMaxY(self.editPhone.frame) + offset, 90, 20);
    
    self.Change.frame = CGRectMake(offset + 300,CGRectGetMaxY(self.editPhone.frame) + offset, 80, 25);
    
    self.error.frame = CGRectMake(offset + 110,CGRectGetMaxY(self.editCountry.frame) + offset, self.bounds.size.width - offset * 2 - 100, 20);
}

- (void)setTopOffset:(CGFloat)topOffset {
    if (_topOffset != topOffset) {
        _topOffset = topOffset;
        [self setNeedsLayout];
    }
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

@end
