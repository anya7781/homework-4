//
//  CountryView.m
//  Lection6
//
//  Created by Admin on 14.11.16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "CountryView.h"
#import "SearchController.h"

@implementation CountryView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        
        _table = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        [self addSubview:_table];
        
        _searchField = [[UITextField alloc] init];
        [self addSubview:_searchField];
        
        _searchButton = [[UIButton alloc] init];
        
        [_searchButton setImage:[UIImage imageNamed:@"search"] forState:UIControlStateNormal];
        
        [self addSubview:_searchButton];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat offset = 40.0f;
    
    self.table.frame = CGRectMake(offset, offset + 30, self.bounds.size.width - offset, self.bounds.size.height);
    
    self.searchField.frame = CGRectMake(offset, offset + 40, 250, 30);
    self.searchField.borderStyle = UITextBorderStyleRoundedRect;
    
    
    self.searchButton.frame = CGRectMake(offset + 270, offset + 40, 30, 30);
    
    
}




@end
