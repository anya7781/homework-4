//
//  ViewController.h
//  Lection6
//
//  Created by Vladislav Grigoriev on 03/11/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Model.h"
#import "EditViewController.h"

@interface ViewController : UIViewController

@property (nonatomic, strong) Model *item;


@end

