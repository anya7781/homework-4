//
//  ViewController.m
//  Lection6
//
//  Created by Vladislav Grigoriev on 03/11/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "ViewController.h"
#import "CustomView.h"


@interface ViewController ()

@property (nonatomic, strong) CustomView *customView;

@end


@implementation ViewController

- (void)loadView {
    
    self.customView = [[CustomView alloc] init];
    [self setView:self.customView];
    [_item load];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.customView.backgroundColor = [UIColor whiteColor];
    
    
    if (_item == nil) {
        _customView.Name.text = @"First Name";
        _customView.SurName.text = @"Last Name";
        _customView.FatherName.text = @"Middle Name";
        _customView.Email.text = @"E-mail";
        _customView.Phone.text = @"Phone";
        _customView.Country.text = @"Country";
        [_customView.Image setImage:[UIImage imageNamed:@"addIcom"] forState:UIControlStateNormal];
    }
    
    else {
        _customView.Name.text = _item.Name;
        _customView.SurName.text = _item.SurName;
        _customView.FatherName.text = _item.FatherName;
        _customView.Email.text = _item.Email;
        _customView.Phone.text = _item.Phone;
        _customView.Country.text = _item.Country;
        
        [_customView.Image setImage:[UIImage imageNamed:_item.NameImage] forState:UIControlStateNormal];
        
    }
    
    [self.customView.Edit addTarget:self action:@selector(editItem:) forControlEvents:UIControlEventTouchUpInside];
    
}

- (void)editItem:(id)sender {
    
    EditViewController *editController = [[EditViewController alloc] init];
    editController.item = _item;
    editController.b = YES;
    
    [self.navigationController pushViewController:editController animated:YES];
}

@end
