//
//  CountryController.h
//  Lection6
//
//  Created by Admin on 14.11.16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Model.h"

@interface CountryController : UIViewController

@property (nonatomic, copy) Model *item;
@property (nonatomic, strong) NSMutableArray *countries;

@end
