//
//  Item.m
//  Lection6
//
//  Created by Admin on 12.11.16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "Model.h"

@implementation Model

- (instancetype) init {
    self = [super init];
    self.NameImage = @"addIcom";
    return self;
}




- (id)copyWithZone:(NSZone *)zone {
    Model *item = [[Model alloc] init];
    item.Name = self.Name;
    item.SurName = self.SurName;
    item.FatherName = self.FatherName;
    item.Email = self.Email;
    item.Phone = self.Phone;
    item.Country = self.Country;
    item.NameImage = self.NameImage;
    return item;
}


- (nullable instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    if (self) {
        _Name = [aDecoder decodeObjectForKey:@"Name"];
        _SurName = [aDecoder decodeObjectForKey:@"SurName"];
        _FatherName = [aDecoder decodeObjectForKey:@"FatherName"];
        _Email = [aDecoder decodeObjectForKey:@"Email"];
        _Phone = [aDecoder decodeObjectForKey:@"Phone"];
        _Country = [aDecoder decodeObjectForKey:@"Country"];
        _NameImage = [aDecoder decodeObjectForKey:@"NameImage"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.Name forKey:@"Name"];
    [aCoder encodeObject:self.SurName forKey:@"SurName"];
    [aCoder encodeObject:self.FatherName forKey:@"FatherName"];
    [aCoder encodeObject:self.Email forKey:@"Email"];
    [aCoder encodeObject:self.Phone forKey:@"Phone"];
    [aCoder encodeObject:self.Country forKey:@"Country"];
    [aCoder encodeObject:self.NameImage forKey:@"NameImage"];
}

- (void)save {
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:self.item];
    
    NSString *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    
    [data writeToFile:[path stringByAppendingPathComponent:@"MyFile"] atomically:YES];
}

- (void)load {
    _item = [[Model alloc] init];
    NSString *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    
    NSData *data = [NSData dataWithContentsOfFile:[path stringByAppendingPathComponent:@"MyFile"]];
    
    if (data) {
        _item = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    }
}

@end
